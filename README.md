实现文字顶部对齐（Top Alignment）的UILabel。原有的 UILabel 中的文字对齐方式不能顶部对齐，只能中部以及左右对齐。这份代码实现的 MCTopAligningLabel 类可将文字顶部对齐。在下面截图中，两个label的高度都是150，左边 MCTopAligningLabel 的文字是顶部对齐，而右边 UILabel 的文字是中部对齐。	
